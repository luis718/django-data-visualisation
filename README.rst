Django Wrapper Application with various charts using D3.js
========================================================


Installation
------------

Install, upgrade and uninstall django-nvd3 with these commands::

    $ pip install django-nvd3
    $ pip install --upgrade django-nvd3
    $ pip uninstall django-nvd3

Then edit settings.py from your django project and add 'django_nvd3' in your 'INSTALLED_APPS' setting.


Dependencies
------------

    $ npm install -g bower
    $ pip install django-bower
    $ python manage.py bower_install
    $ python manage.py collectstatic

PieChart Example
--------------------------------

To achieve this, edit the view.py, prepare the data that will be displayed::

    xdata = ["Apple", "Apricot", "Avocado", "Banana", "Boysenberries", "Blueberries", "Dates", "Grapefruit", "Kiwi", "Lemon"]
    ydata = [52, 48, 160, 94, 75, 71, 490, 82, 46, 17]
    chartdata = {'x': xdata, 'y': ydata}
    charttype = "pieChart"
    chartcontainer = 'piechart_container'
    data = {
        'charttype': charttype,
        'chartdata': chartdata,
        'chartcontainer': chartcontainer,
        'extra': {
            'x_is_date': False,
            'x_axis_format': '',
            'tag_script_js': True,
            'jquery_on_ready': False,
        }
    }
    return render_to_response('piechart.html', data)


Render the template 'piechart.html' with a dictionary 'data' which contains 'charttype' and 'chartdata'.
'extra' will contains a list of additional settings::

    * ``x_is_date`` - if enabled the x-axis will be display as date format
    * ``x_axis_format`` - set the x-axis date format, ie. "%d %b %Y"
    * ``tag_script_js`` - if enabled it will add the javascript tag '<script>'
    * ``jquery_on_ready`` - if enabled it will load the javascript only when page is loaded
        this will use jquery library, so make sure to add jquery to the template.
    * ``color_category`` - Define color category (eg. category10, category20, category20c)


The template piechart.html could look like this::

    {% load nvd3_tags %}
    <head>
        {% include_chart_jscss %}
        {% load_chart charttype chartdata chartcontainer extra %}
    </head>
    <body>
        <h1>Fruits vs Calories</h1>
        {% include_container chartcontainer 400 600 %}
    </body>

Include the Javascript and CSS code for D3/NVD3.
Prepare and display the javascript code needed to render the pieChart::

    {% load_chart charttype chartdata "piechart_container" extra %}

The result will be a beautiful and interactive chart:

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/piechart_fruits_vs_calories.png


Demo of NVD3
-----------------

Charts list:

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/lineWithFocusChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/lineChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/multiBarChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/pieChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/stackedAreaChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/multiBarHorizontalChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/linePlusBarChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/cumulativeLineChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/discreteBarChart.png

.. image:: https://raw.github.com/areski/django-nvd3/master/docs/source/_static/screenshot/scatterChart.png


